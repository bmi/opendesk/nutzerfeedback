# Nutzerfeedback

In einem Zeitraum von knapp vier Monaten (Juli-Oktober 2023) haben wir den openDesk gemeinsam mit unseren Erprobungspartnerinnen und -partnern kontinuierlich getestet. Aus mehreren Bundesbehörden, die zu unseren Testpartnerinnen gehören, nahmen insgesamt rund 60 Vertreterinnen und Vertreter aktiv an einer Reihe von Tests teil. Testgegenstand für die Mitwirkenden war ein Abbild der zukünftigen Anwendungsumgebung, welche dediziert für die Maßnahmen zur Nutzerforschung bereitgestellt wurde. Folgende Anwendungen von openDesk wurden getestet:

- Open-Xchange (E-Mail und Kalender)
- Nextcloud (Dateiablage und -sharing)
- Collabora (Office-Anwendungen) 
- Jitsi (Videokonferenz)
- XWiki (Wissensmanagement)
- OpenProject (Projektmanagement)

Ergänzend zu den Anwendungen wurden insbesondere die übergreifenden Bereiche der Umgebung, wie z. B. die Navigation, das Dashboard (Portal), die Einstellungen sowie die Interaktion zwischen den Anwendungen getestet. Konkret wurden insgesamt vier umfangreiche Methoden aus der Nutzerforschung zur Erhebung und Dokumentation des Nutzerfeedbacks durchgeführt:

- Tagebuchstudie
- Retro-Interviews
- Usability Tests
- heuristische Analyse

Neben dem Gesamteindruck zum openDesk sowie zu einzelnen Anwendungen lag der Fokus dieses fortlaufenden Austausches auf verschiedenen Parametern, wie z. B. auf der Funktionalität, Stabilität und der Nutzerfreundlichkeit. Aus diesem sehr konstruktiven Zusammenspiel zwischen der Nutzerforschung und den Erprobungspartnerinnen und -partnern konnte eine Vielzahl von Rückmeldungen, Anmerkungen, Lob, Kritik und Verbesserungsvorschlägen gewonnen werden.

Diese Seite soll eine detaillierte Einsicht in das erhobene Nutzerfeedback bereitstellen. Zu diesem Zweck wurde das erhobene Feedback in Form von einzelnen Issues im Sinne von Gitlab erstellt und auf dieser Seite hochgeladen:

🔗 https://gitlab.opencode.de/bmi/opendesk/nutzerfeedback/-/issues

Die einzelnen Issues wurden jeweils mit zuvor definierten Labels versehen, welche nach verschiedenen Parametern definiert wurden. So gibt es beispielsweise ein Label pro Modulhersteller, Labels für die verschiedenen Feedbacktypen oder für die jeweils festgelegte Priorität zur Optimierung von openDesk. Die Labels sollen dabei unterstützen, die Issues thematisch zu kategorisieren und somit besser auffindbar zu machen. Eine Liste mit allen Labels ist hier zu finden:

🔗 https://gitlab.opencode.de/groups/bmi/opendesk/-/labels

Eine Übersicht mit u. a. ausführlichen Erläuterungen zu den Labels kann folgendem Dokument entnommen werden:

🔗 https://gitlab.opencode.de/bmi/opendesk/info/-/blob/main/ISSUE_MANAGEMENT.md

Jetzt bist du an der Reihe: Mach mit und beteilige dich an der Optimierung von openDesk!
